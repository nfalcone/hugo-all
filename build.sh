#!/bin/sh

#purge old
rm -rf ./public/*

#build new
hugo -t new.nfalcone.net
git add --all
git commit -a --allow-empty-message -m ""
git push origin master

#ftp up copy over
#upload to webhost
ncftpput -R -v -u "defaultnfalcone-net" -p "etphoGaP@WS9A" example.com /web/ public/*
#push to gitlab site
cd public
git remote add public https://gitlab.com/nfalcone/nfalcone.gitlab.io
git add --all
git commit -a --allow-empty-message -m ""
git push public