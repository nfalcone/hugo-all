I love backupninja + duplicity, if you have read one of my previous articles you will know that it is my primary method of backing up my servers and laptops.

Recently I switched one of my work laptops over to Solus Liux 3.999 https://getsol.us/home/

Backupninja (not surprisingly) was not in the stock repositories for eopkg.  This is pretty typical it seems to only be in default repos for Debian and Ubuntu.
Luckily backupninja is a very portable piece of software so it should be easy to install on Solus, here is how I did it.


First we need to install some pre-req packages from the Solus repo.

This will install developer tools we need, (Debian's build-essential equivailent)
```
sudo eopkg it -c system.devel
```

Install tools that backupninja needs
```
sudo eopkg install bash gawk fish zsh duplicity rsync gzip cryptsetup dialog
```

Now to get the source and build.

```
git clone https://0xacab.org/riseuplabs/backupninja
```

and

```
./autogen.sh
./configure
make
sudo make install
```

And that is it, your configs go in /etc/backup.d like usual and run `/usr/local/sbin/ninjahelper`
