+++
date = "2019-02-18T07:16:56-04:00"
draft = false
title = "Dell H330 storage disapearing, Host Not Responding in Vcenter"

+++

Recently got 4 new [Dell R7425](https://www.dell.com/en-us/work/shop/povw/poweredge-r7425) servers, all running [ESXI 6.5U2](https://www.dell.com/support/home/us/en/04/drivers/driversdetails?driverid=ckc15).  After a few days we were getting hosts intermittenly going *Not Responding* in vcenter.
I knew something was up, upon checking the console via DRAC trying to turn on SSH via the ESXI console screen took about 30 minutes.

Before involving Vmware and Dell Support, the only solution seemed to be powering off the VMs and reseting the host.

Vmware discovered the issue on our hosts, when looking for logs `/var/log` was not found.  The whole local datastore was gone (VMs are stored on iscsi SAN).  Checking the DRAC the card and array had all green che$

Dell noticed our storage firmware is up to date but our storage driver is a bit old (though still supported by Vmware).


Dell recommend we update to the latest driver [https://my.vmware.com/web/vmware/details?downloadGroup=DT-ESX65-AVAGO-LSI-MR3-77051000-1OEM&productId=614](https://my.vmware.com/web/vmware/details?downloadGroup=DT-ESX65-AVAGO-LSI-MR3-77051000-1OEM&productId=614)

Turn SSH on the host (this will take awhile) and copy the file above to `/tmp`.

Then run

```
esxcli software vib install -d /tmp/VMW-ESX-6.5.0-lsi_mr3-7.705.10.00-offline_bundle-11658035.zip
```

If everything goes right you will get:
```
Installation Result
   Message: The update completed successfully, but the system needs to be rebooted for the changes to be effective.
   Reboot Required: true
   VIBs Installed: Avago_bootbank_lsi-mr3_7.705.10.00-1OEM.650.0.0.4598673
   VIBs Removed: Avago_bootbank_lsi-mr3_7.703.18.00-1OEM.650.0.0.4598673
   VIBs Skipped:

```

Reboot the host you can then verify in the drac that driver is a newer version.
***After update:***
![](/images/drac-after.png)
***Prior:***
![](/images/drac-before.png)

