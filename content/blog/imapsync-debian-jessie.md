+++
date = "2017-04-27T07:16:56-04:00"
draft = false
title = "Imapsync On Debian Jessie"

+++

Because of some licensing dispute/change Imapsync is no longer in Debian or Ubuntu default repos and has to be compiled from source.

There are a lot of sites out there with instructions to get it working on a Debian or ubuntu server but none of them ever totally worked for me. I compiled these instructions after a bit of trial and error and have migrated a bunch of mail servers with this great tool.

Install pre-req librarys
```
sudo apt-get install build-essential libdate-manip-perl libterm-readkey-perl libterm-readkey-perl libdigest-hmac-perl libdigest-hmac-perl libdate-manip-perl libmail-imapclient-perl makepasswd rcs perl-doc git libunicode-string-perl libio-tee-perl libauthen-ntlm-perl \
libcrypt-openssl-rsa-perl libjson-perl liblwp-online-perl
```
More perl pre-reqs from cpan
```
sudo cpan Data::Uniqid File::Copy::Recursive JSON::WebToken Readonly Test::MockObject Test::Pod IO::Socket::INET6
```
Pull in source
```
git clone https://github.com/imapsync/imapsync
```
And then compile
```
cd imapsync
sudo make install
```
Then run like below:
```
imapsync -ssl1 -ssl2 --host1 server1.com --user1 [email protected] --password1 Secret1 --host2 server2.com --user2 [email protected] --password2 Secret2
```

