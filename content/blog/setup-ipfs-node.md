+++
date = "2019-01-06T07:16:56-04:00"
draft = false
title = "Setting up your own IPFS node"

+++
![](/images/ipfs-logo.png)

IPFS is a peer-to-peer hypermedia protocol
to make the web faster, safer, and more open.  It is similar to combining HTTP publishing with bittorrent.  In order to participate and publish files one needs to run a node.  These instructions assume running as root, so add sudo if not.

Get latest version of ipfs node software and install.
```
wget https://dist.ipfs.io/go-ipfs/v0.4.18/go-ipfs_v0.4.18_linux-amd64.tar.gz
tar zxvf go-ipfs_v0.4.18_linux-amd64.tar.gz
mv go-ipfs/ipfs /usr/local/bin/
chmod +x /usr/loca/bin/ipfs
```

create a user for ipfs to run as
```
adduser ipfs
ipfs init --profile server
ipfs daemon
```

You can start ipfs in two ways; simple shell script or a systemd service file.

create a file like `start_ipfs.sh` with the following:
```
#!/bin/sh
su - ipfs -c "/usr/bin/screen -d -m -fa -S ipfs /usr/local/bin/ipfs daemon"
```
then run it:
```
chmod +x start_ipfs.sh
./start_ipfs.sh
```

or for systemd

create this file `/etc/systemd/system/ipfs.service` with the following:
```
[Unit]
Description=IPFS Daemon
After=syslog.target network.target remote-fs.target nss-lookup.target

[Service]
Type=simple
ExecStart=/usr/local/bin/ipfs daemon --enable-namesys-pubsub
User=ipfs

[Install]
WantedBy=multi-user.target
```
Then run:
```
systemctl daemon-reload
systemctl enable ipfs
systemctl start ipfs
```

More information [here](https://michalzalecki.com/set-up-ipfs-node-on-the-server/) and [here](https://github.com/ipfs/papers/raw/master/ipfs-cap2pfs/ipfs-p2p-file-system.pdf)
