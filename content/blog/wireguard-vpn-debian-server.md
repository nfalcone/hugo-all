+++
date = "2019-01-07T00:00:00-00:00"
draft = false
title = "Install Wireguard VPN on Debian 9 Server"

+++


WireGuard is a next-gen VPN server that is supposed to be more simple to configure than OpenVPN, while being faster and beter on latency in the tunnel.
Currently it is not listed as a stable piece of software, but in its current state it is still useful and useable.

This article was written and tested using Debian 9 on a [Vultr](https://www.vultr.com/?ref=6812785) VPS, but should work on any deb based distro.

## Install WireGuard

First we need to install some pre-reqs: (because wireguard integrates into the kernel we need kernel headers to compile)

```
apt-get install linux-headers libmnl-dev linux-headers-$(uname -r) build-essential make git
```

Next we will add the unstable repo and pin the wireguard package from that repo.  An alternative to this would be to pull and compile right from the [wireguard git repo](https://git.zx2c4.com/WireGuard) and compile manually.

```
echo "deb http://deb.debian.org/debian/ unstable main" > /etc/apt/sources.list.d/unstable-wireguard.list
printf 'Package: *\nPin: release a=unstable\nPin-Priority: 150\n' > /etc/apt/preferences.d/limit-unstable
apt-get update
apt-get install wireguard-dkms wireguard-tools
```

Enable the wireguard kernel module and verify
```
modprobe wireguard && lsmod | grep wireguard
```

## Configure WireGuard

First we have to generate a key
```
wg genkey
```

Then place the following text into `/etc/wireguard/wg0.conf` with your favorite *$EDITOR*
```
[Interface]
PrivateKey = <PUT HERE THE KEY JUST GENERATED>
Address = 10.0.0.1/24, fd86:ea04:1115::1/64
ListenPort = 51820
PostUp = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE; ip6tables -A FORWARD -i wg0 -j ACCEPT; ip6tables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE; ip6tables -D FORWARD -i wg0 -j ACCEPT; ip6tables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
SaveConfig = true
```

Next it would be a good idea to enable packet forwarding (as with any VPN service).

Open this file `/etc/sysctl.conf` and change the following:
```
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1
```

You can reboot your server or run `sysctl -p` for these changes to take affect.

## Start and enable WireGuard

Now we want WireGuard to start and to start on boot up.  We run the following:

```
wg-quick up wg0
systemctl enable wg-quick@wg0
```

And that is it server side!

## Client Setup

We need to find the public key for the client, below we find it from the private key earlier and then save in the file `public`

```
wg pubkey < private > public
cat public
```

Lastly we configure the server to accept and assign the client: **(back on the server)**
```
wg set wg0 peer <CLIENT/PEER PUBLIC KEY> allowed-ips 10.0.0.5,fd86:ea04:1115::5
```

Make sure you set your config on the client to use the public key from the server from the `public` file and that your IPs match what you are assigning and what is in the client config.

## Troubleshooting Commands
```
wg show
ifconfig
```

You can install a client the same way on a Linux PC as above, for Windows I recommend the [TunSafe](https://tunsafe.com/) client, though beware it is not opensource.

More information can be found [here](https://www.vultr.com/docs/create-a-secure-connection-between-two-servers-using-wireguard-on-ubuntu) and [here](https://technofaq.org/posts/2017/10/how-to-setup-wireguard-vpn-on-your-debian-gnulinux-server-with-ipv6-support/)

