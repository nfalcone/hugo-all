+++
date = "2017-10-19T07:16:56-04:00"
draft = false
title = "OpenBSD 6.2 web server with httpd and certbot"
+++

This was tested on OpenBSD 6.2, your experience may very depending on your version.


httpd is chroot'd so the `/htdocs` in the config below refers to `/var/www/htdocs` on the actual server filesystem.

Enable *httpd*

```
echo 'httpd_flags=""' > /etc/rc.conf.local
```


Now install certbot and request our certificate: (In previous OpenBSD versions it was called acmeclient)
```
echo 'export PKG_PATH=https://ftp5.usa.openbsd.org/pub/OpenBSD/6.2/packages/amd64/' >> /root/.profile
pkg_add -v certbot

certbot certonly --standalone --preferred-challenges http -d example.com
```



Next let's create our httpd config

```
ext_ip="192.168.1.18"  #your IP here

server "default" {
        listen on $ext_ip port 80
}
types {
        text/css css ;
        text/html htm html ;
        text/txt txt ;
        image/gif gif ;
        image/jpeg jpg jpeg ;
        image/png png ;
        application/javascript js ;
        application/xml xml ;
}
server "nfalcone.net" {
        listen on $ext_ip port 80
        listen on $ext_ip tls port 443
        tls certificate "/etc/letsencrypt/live/example.com/fullchain.pem"
        tls key "/etc/letsencrypt/live/example.com/privkey.pem"
        root "/htdocs/example.com" #whatever folder you made under /var/www/htdocs
		directory auto index  #if you want directory listings
}
        
```

Start your web server!

```
/etc/rc.d/httpd start
```



Next configure certbot auto renewals
```
crontab -e
```

and paste in
```
0       *       *       *       *       /usr/local/bin/certbot renew
```

lastly edit `/etc/letsencrypt/renewal/example.com.conf`

and add
```
renew_hook = /etc/rc.d/httpd restart
```

That is it you are all set!
